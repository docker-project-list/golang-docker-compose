module github.com/golangid/menekel

go 1.12

require (
	cloud.google.com/go/compute v1.1.0 // indirect
	cloud.google.com/go/spanner v1.29.0 // indirect
	cloud.google.com/go/storage v1.19.0 // indirect
	github.com/Azure/go-autorest/autorest/adal v0.9.18 // indirect
	github.com/ClickHouse/clickhouse-go v1.5.3 // indirect
	github.com/Masterminds/squirrel v1.2.0
	github.com/aws/aws-sdk-go v1.42.44 // indirect
	github.com/aws/aws-sdk-go-v2/feature/s3/manager v1.9.1 // indirect
	github.com/bxcodec/faker v2.0.1+incompatible
	github.com/bxcodec/faker/v3 v3.5.0
	github.com/cenkalti/backoff/v4 v4.1.2 // indirect
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/cncf/udpa/go v0.0.0-20220112060539-c52dc94e7fbe // indirect
	github.com/cncf/xds/go v0.0.0-20220121163655-4a2b9fdd466b // indirect
	github.com/cockroachdb/cockroach-go v2.0.1+incompatible // indirect
	github.com/cockroachdb/cockroach-go/v2 v2.2.6 // indirect
	github.com/cznic/mathutil v0.0.0-20181122101859-297441e03548 // indirect
	github.com/denisenkom/go-mssqldb v0.12.0 // indirect
	github.com/docker/distribution v2.7.1+incompatible // indirect
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-units v0.4.0 // indirect
	github.com/envoyproxy/go-control-plane v0.10.1 // indirect
	github.com/envoyproxy/protoc-gen-validate v0.6.3 // indirect
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-sql-driver/mysql v1.6.0
	github.com/go-stack/stack v1.8.1 // indirect
	github.com/gocql/gocql v0.0.0-20211222173705-d73e6b1002a7 // indirect
	github.com/golang-jwt/jwt/v4 v4.2.0 // indirect
	github.com/golang-migrate/migrate v3.5.4+incompatible
	github.com/golang-migrate/migrate/v4 v4.15.1 // indirect
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/google/flatbuffers v2.0.5+incompatible // indirect
	github.com/google/go-github v17.0.0+incompatible // indirect
	github.com/google/go-github/v35 v35.3.0 // indirect
	github.com/gorilla/context v1.1.1 // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.0 // indirect
	github.com/jackc/pgx/v4 v4.14.1 // indirect
	github.com/k0kubun/pp v3.0.1+incompatible // indirect
	github.com/klauspost/compress v1.14.2 // indirect
	github.com/ktrysmt/go-bitbucket v0.9.36 // indirect
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/echo/v4 v4.1.17
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/lib/pq v1.10.4 // indirect
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-ieproxy v0.0.3 // indirect
	github.com/mattn/go-sqlite3 v1.14.11 // indirect
	github.com/mitchellh/mapstructure v1.4.3 // indirect
	github.com/mutecomm/go-sqlcipher/v4 v4.4.2 // indirect
	github.com/nakagami/firebirdsql v0.9.3 // indirect
	github.com/neo4j/neo4j-go-driver v1.8.3 // indirect
	github.com/pierrec/lz4/v4 v4.1.12 // indirect
	github.com/shopspring/decimal v1.3.1 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/snowflakedb/glog v0.0.0-20180824191149-f5055e6f21ce // indirect
	github.com/snowflakedb/gosnowflake v1.6.6 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/cobra v1.0.0
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.6.2
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/stretchr/testify v1.7.0
	github.com/xanzy/go-gitlab v0.54.4 // indirect
	github.com/xdg-go/scram v1.1.0 // indirect
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	github.com/youmark/pkcs8 v0.0.0-20201027041543-1326539a0a0a // indirect
	gitlab.com/cznic/ebnf2y v1.0.0 // indirect
	gitlab.com/cznic/golex v1.0.0 // indirect
	go.mongodb.org/mongo-driver v1.8.3 // indirect
	go.uber.org/atomic v1.9.0 // indirect
	golang.org/x/crypto v0.0.0-20220131195533-30dcbda58838 // indirect
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd // indirect
	golang.org/x/sys v0.0.0-20220128215802-99c3d69c2c27 // indirect
	golang.org/x/time v0.0.0-20211116232009-f0f3c7e86c11 // indirect
	golang.org/x/tools v0.1.9 // indirect
	google.golang.org/api v0.66.0 // indirect
	google.golang.org/genproto v0.0.0-20220201184016-50beb8ab5c44 // indirect
	google.golang.org/grpc v1.44.0 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.31.0
	gopkg.in/ini.v1 v1.54.0 // indirect
	lukechampine.com/uint128 v1.2.0 // indirect
	modernc.org/ccgo/v3 v3.15.13 // indirect
	modernc.org/db v1.0.4 // indirect
	modernc.org/ebnfutil v1.0.0 // indirect
	modernc.org/golex v1.0.1 // indirect
	modernc.org/lldb v1.0.4 // indirect
	modernc.org/ql v1.4.1 // indirect
	modernc.org/sqlite v1.14.5 // indirect
)
